module github.com/ericvh/cpu

go 1.17

require (
	github.com/gliderlabs/ssh v0.3.3
	github.com/hugelgupf/p9 v0.1.1-0.20210528120703-cc05b77b314b
	github.com/kevinburke/ssh_config v1.1.0
	github.com/u-root/u-root v0.9.0
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
	golang.org/x/sys v0.0.0-20220610221304-9f5ed59c137d
)

require (
	github.com/creack/pty v1.1.15
	github.com/hashicorp/go-multierror v1.1.1
	github.com/mdlayher/vsock v1.1.1
)

require (
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/klauspost/compress v1.10.6 // indirect
	github.com/klauspost/pgzip v1.2.4 // indirect
	github.com/mdlayher/socket v0.2.0 // indirect
	github.com/u-root/uio v0.0.0-20220204230159-dac05f7d2cb4 // indirect
	github.com/ulikunitz/xz v0.5.8 // indirect
	github.com/vishvananda/netlink v1.1.1-0.20211118161826-650dca95af54 // indirect
	github.com/vishvananda/netns v0.0.0-20210104183010-2eb08e3e575f // indirect
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
)
